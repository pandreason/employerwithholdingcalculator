﻿export interface WithholdingAllowancesMap {
    [withHoldingPeriod: string]: number;
}