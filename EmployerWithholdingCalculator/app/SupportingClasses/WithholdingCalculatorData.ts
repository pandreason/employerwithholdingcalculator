﻿import { Injectable } from "@angular/core";

@Injectable()
export class WithholdingCalculatorData {
    payFrequency: string = 'Biweekly';
    grossIncome: number = 0.00;
    taxFilingStatus: string = "Single";
    numberOfAllowances: number = 0;
    taxesWithheld: number = 0;
}