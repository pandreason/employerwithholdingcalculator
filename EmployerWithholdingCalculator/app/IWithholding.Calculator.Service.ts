﻿export interface ITaxWitholdingCalculatorService {
    biweeklyWitholdingAllowance: number;

    calculateWithholding(numAllowances: number, grossIncome: number, filingStatus: string): number;
}