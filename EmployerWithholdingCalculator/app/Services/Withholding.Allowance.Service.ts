﻿import { Injectable } from "@angular/core";
import { WithholdingAllowancesMap } from "../SupportingClasses/WithholdingAllowancesMap";

@Injectable()
export class WithholdingAllowanceService {
    private Allowances: WithholdingAllowancesMap = {
        'Weekly': 77.90,
        'Biweekly': 155.80,
        'Semimonthly': 168.80,
        'Monthly': 337.50,
        'Quarterly': 1012.50,
        'Semiannually': 2025.00,
        'Annually': 4050.00
    };

    getAllowances(): WithholdingAllowancesMap {
        return this.Allowances;
    }

    getPayFrequencies(): string[] {
        return Object.keys(this.Allowances);
    }
}