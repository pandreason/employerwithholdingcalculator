﻿import { Injectable } from "@angular/core";

@Injectable()

export class FilingStatusesService {
    getStatuses(): string[] {
        return ["Single", "Married filing jointly", "Married filing separate", "Head of Household", "Qualifying widow(er)"];
    }
}