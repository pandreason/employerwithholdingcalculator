﻿import { Injectable } from "@angular/core";
import { WithholdingAllowancesMap } from "../SupportingClasses/WithholdingAllowancesMap";
import { WithholdingAllowanceService } from "./Withholding.Allowance.Service";
import { WithholdingTableService } from "./Withholding.Table.Service";
import { WithholdingCalculatorData } from "../SupportingClasses/WithholdingCalculatorData"

@Injectable()
export class TaxWitholdingCalculatorService {

    private allowances: WithholdingAllowancesMap;

    constructor(private withholdingAllowancesService: WithholdingAllowanceService,
                private withholdingTableService: WithholdingTableService) {
        this.allowances = this.withholdingAllowancesService.getAllowances();
    }

    calculateWithholding(withholdingCalculatorData: WithholdingCalculatorData): void {
        withholdingCalculatorData.taxesWithheld = 0.00;

        var oneAllowance = this.allowances[withholdingCalculatorData.payFrequency];

        var amountAllowedOnW4 = oneAllowance * withholdingCalculatorData.numberOfAllowances;

        var amountSubjectToWithholding = withholdingCalculatorData.grossIncome - amountAllowedOnW4;

        var withholdingTable = this.withholdingTableService.getWitholdingTable(withholdingCalculatorData.taxFilingStatus, withholdingCalculatorData.payFrequency);

        var keys = Object.keys(withholdingTable);

        var myKey:number = 0;

        for (var key of keys)
        {
            if (amountSubjectToWithholding >= +key)
                myKey = +key;
            else
                break;

        }

        if (myKey > 0)
            withholdingCalculatorData.taxesWithheld = withholdingTable[myKey](amountSubjectToWithholding);
    }
}