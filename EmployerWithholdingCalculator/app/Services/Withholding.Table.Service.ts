﻿import { Injectable } from "@angular/core";

@Injectable()
export class WithholdingTableService
{
    private withholdingTable: { [index: number]: (num: number) => number } = {};

    getWitholdingTable(filingStatus: string, payFrequency: string) {
        switch (filingStatus)
        {
            case "Single":
            case "Head of Household":
                switch (payFrequency) {
                    case "Weekly":
                        this.withholdingTable = {};
                        this.withholdingTable[43] = (num) => { return 0 + ((num - 43) * .1); };
                        this.withholdingTable[222] = (num) => { return 17.90 + ((num - 222) * .15); };
                        this.withholdingTable[767] = (num) => { return 99.65 + ((num - 767) * .25); };
                        this.withholdingTable[1796] = (num) => { return 356.90 + ((num - 1796) * .28); };
                        this.withholdingTable[3700] = (num) => { return 890.02 + ((num - 3700) * .33); };
                        this.withholdingTable[7992] = (num) => { return 2306.38 + ((num - 7992) * .35); };
                        this.withholdingTable[8025] = (num) => { return 2317.93 + ((num - 8025) * .396); };
                        break;
                    case "Biweekly":
                        this.withholdingTable = {};
                        this.withholdingTable[87] = (num) => { return 0 + ((num - 87) * .1); };
                        this.withholdingTable[443] = (num) => { return 35.60 + ((num - 443) * .15); };
                        this.withholdingTable[1535] = (num) => { return 199.40 + ((num - 1535) * .25); };
                        this.withholdingTable[3592] = (num) => { return 713.65 + ((num - 3592) * .28); };
                        this.withholdingTable[7400] = (num) => { return 1779.89 + ((num - 7400) * .33); };
                        this.withholdingTable[15985] = (num) => { return 4612.94 + ((num - 15985) * .35); };
                        this.withholdingTable[16050] = (num) => { return 4635.69 + ((num - 16050) * .396); };
                        break;
                    case "Semimonthly":
                        this.withholdingTable = {};
                        this.withholdingTable[94] = (num) => { return 0 + ((num - 94) * .1); };
                        this.withholdingTable[480] = (num) => { return 38.60 + ((num - 480) * .15); };
                        this.withholdingTable[1663] = (num) => { return 216.05 + ((num - 1663) * .25); };
                        this.withholdingTable[3892] = (num) => { return 773.30 + ((num - 3892) * .28); };
                        this.withholdingTable[8017] = (num) => { return 1928.30 + ((num - 8017) * .33); };
                        this.withholdingTable[17317] = (num) => { return 4997.30 + ((num - 17317) * .35); };
                        this.withholdingTable[17388] = (num) => { return 5022.15 + ((num - 17388) * .396); };
                        break;
                    case "Monthly":
                        this.withholdingTable = {};
                        this.withholdingTable[188] = (num) => { return 0 + ((num - 188) * .1); };
                        this.withholdingTable[960] = (num) => { return 77.20 + ((num - 960) * .15); };
                        this.withholdingTable[3325] = (num) => { return 431.95 + ((num - 3325) * .25); };
                        this.withholdingTable[7783] = (num) => { return 1546.45 + ((num - 7783) * .28); };
                        this.withholdingTable[16033] = (num) => { return 3856.45 + ((num - 16033) * .33); };
                        this.withholdingTable[34633] = (num) => { return 9994.45 + ((num - 34633) * .35); };
                        this.withholdingTable[34775] = (num) => { return 10044.15 + ((num - 34775) * .396); };
                        break;
                    case "Quarterly":
                        this.withholdingTable = {};
                        this.withholdingTable[563] = (num) => { return 0 + ((num - 563) * .1); };
                        this.withholdingTable[2881] = (num) => { return 231.80 + ((num - 2881) * .15); };
                        this.withholdingTable[9975] = (num) => { return 1295.90 + ((num - 9975) * .25); };
                        this.withholdingTable[23350] = (num) => { return 4639.65 + ((num - 23350) * .28); };
                        this.withholdingTable[48100] = (num) => { return 11569.65 + ((num - 48100) * .33); };
                        this.withholdingTable[103900] = (num) => { return 29983.65 + ((num - 103900) * .35); };
                        this.withholdingTable[104325] = (num) => { return 30132.40 + ((num - 104325) * .396); };
                        break;
                    case "Semiannually":
                        this.withholdingTable = {};
                        this.withholdingTable[1125] = (num) => { return 0 + ((num - 1125) * .1); };
                        this.withholdingTable[5763] = (num) => { return 463.80 + ((num - 5763) * .15); };
                        this.withholdingTable[19950] = (num) => { return 2591.85 + ((num - 19950) * .25); };
                        this.withholdingTable[46700] = (num) => { return 9279.35 + ((num - 46700) * .28); };
                        this.withholdingTable[96200] = (num) => { return 23139.35 + ((num - 96200) * .33); };
                        this.withholdingTable[207800] = (num) => { return 59967.35 + ((num - 207800) * .35); };
                        this.withholdingTable[208650] = (num) => { return 60264.85 + ((num - 208650) * .396); };
                        break;
                    case "Annually":
                        this.withholdingTable = {};
                        this.withholdingTable[2250] = (num) => { return 0 + ((num - 2250) * .1); };
                        this.withholdingTable[11525] = (num) => { return 927.50 + ((num - 11525) * .15); };
                        this.withholdingTable[39900] = (num) => { return 5183.75 + ((num - 39900) * .25); };
                        this.withholdingTable[93400] = (num) => { return 18558.75 + ((num - 93400) * .28); };
                        this.withholdingTable[192400] = (num) => { return 46278.75 + ((num - 192400) * .33); };
                        this.withholdingTable[415600] = (num) => { return 119934.75 + ((num - 415600) * .35); };
                        this.withholdingTable[417300] = (num) => { return 120529.75 + ((num - 417300) * .396); };
                        break;
                }
                break;
                
            case "Married filing jointly":
            case "Married filing separate":
            case "Qualifying widow(er)":
                switch (payFrequency) {
                    case "Weekly":
                        this.withholdingTable = {};
                        this.withholdingTable[164] = (num) => { return 0 + ((num - 164) * .1); };
                        this.withholdingTable[521] = (num) => { return 35.70 + ((num - 521) * .15); };
                        this.withholdingTable[1613] = (num) => { return 199.50 + ((num - 1613) * .25); };
                        this.withholdingTable[3086] = (num) => { return 567.75 + ((num - 3086) * .28); };
                        this.withholdingTable[4615] = (num) => { return 995.87 + ((num - 4615) * .33); };
                        this.withholdingTable[8113] = (num) => { return 2150.21 + ((num - 8113) * .35); };
                        this.withholdingTable[9144] = (num) => { return 2511.06 + ((num - 9144) * .396); };
                        break;
                    case "Biweekly":
                        this.withholdingTable = {};
                        this.withholdingTable[329] = (num) => { return 0 + ((num - 329) * .1); };
                        this.withholdingTable[1042] = (num) => { return 71.30 + ((num - 1042) * .15); };
                        this.withholdingTable[3225] = (num) => { return 398.75 + ((num - 3225) * .25); };
                        this.withholdingTable[6171] = (num) => { return 1135.25 + ((num - 6171) * .28); };
                        this.withholdingTable[9231] = (num) => { return 1992.05 + ((num - 9231) * .33); };
                        this.withholdingTable[16227] = (num) => { return 4300.73 + ((num - 16227) * .35); };
                        this.withholdingTable[18288] = (num) => { return 5022.08 + ((num - 18288) * .396); };
                        break;
                    case "Semimonthly":
                        this.withholdingTable = {};
                        this.withholdingTable[356] = (num) => { return 0 + ((num - 356) * .1); };
                        this.withholdingTable[1129] = (num) => { return 77.30 + ((num - 1129) * .15); };
                        this.withholdingTable[3494] = (num) => { return 432.05 + ((num - 3494) * .25); };
                        this.withholdingTable[6685] = (num) => { return 1229.80 + ((num - 6685) * .28); };
                        this.withholdingTable[10000] = (num) => { return 2158.00 + ((num - 10000) * .33); };
                        this.withholdingTable[17579] = (num) => { return 4659.07 + ((num - 17579) * .35); };
                        this.withholdingTable[19813] = (num) => { return 5440.97 + ((num - 19813) * .396); };
                        break;
                    case "Monthly":
                        this.withholdingTable = {};
                        this.withholdingTable[713] = (num) => { return 0 + ((num - 713) * .1); };
                        this.withholdingTable[2258] = (num) => { return 154.50 + ((num - 2258) * .15); };
                        this.withholdingTable[6988] = (num) => { return 864.00 + ((num - 6988) * .25); };
                        this.withholdingTable[13371] = (num) => { return 2459.75 + ((num - 13371) * .28); };
                        this.withholdingTable[20000] = (num) => { return 4315.87 + ((num - 20000) * .33); };
                        this.withholdingTable[35158] = (num) => { return 9318.01 + ((num - 35158) * .35); };
                        this.withholdingTable[39625] = (num) => { return 10881.46 + ((num - 39625) * .396); };
                        break;
                    case "Quarterly":
                        this.withholdingTable = {};
                        this.withholdingTable[2138] = (num) => { return 0 + ((num - 2138) * .1); };
                        this.withholdingTable[6775] = (num) => { return 463.70 + ((num - 6775) * .15); };
                        this.withholdingTable[20963] = (num) => { return 2591.90 + ((num - 20963) * .25); };
                        this.withholdingTable[40113] = (num) => { return 7379.40 + ((num - 40113) * .28); };
                        this.withholdingTable[60000] = (num) => { return 12947.76 + ((num - 60000) * .33); };
                        this.withholdingTable[105475] = (num) => { return 27954.51 + ((num - 105475) * .35); };
                        this.withholdingTable[118875] = (num) => { return 32644.51 + ((num - 118875) * .396); };
                        break;
                    case "Semiannually":
                        this.withholdingTable = {};
                        this.withholdingTable[4275] = (num) => { return 0 + ((num - 4275) * .1); };
                        this.withholdingTable[13550] = (num) => { return 927.50 + ((num - 13550) * .15); };
                        this.withholdingTable[41925] = (num) => { return 5183.75 + ((num - 41925) * .25); };
                        this.withholdingTable[80225] = (num) => { return 14758.75 + ((num - 80225) * .28); };
                        this.withholdingTable[120000] = (num) => { return 25895.75 + ((num - 120000) * .33); };
                        this.withholdingTable[210950] = (num) => { return 55909.25 + ((num - 210950) * .35); };
                        this.withholdingTable[237750] = (num) => { return 65289.25 + ((num - 237750) * .396); };
                        break;
                    case "Annually":
                        this.withholdingTable = {};
                        this.withholdingTable[8550] = (num) => { return 0 + ((num - 8550) * .1); };
                        this.withholdingTable[27100] = (num) => { return 1855.00 + ((num - 27100) * .15); };
                        this.withholdingTable[83850] = (num) => { return 10367.50 + ((num - 83850) * .25); };
                        this.withholdingTable[160450] = (num) => { return 29517.50 + ((num - 160450) * .28); };
                        this.withholdingTable[240000] = (num) => { return 51791.50 + ((num - 240000) * .33); };
                        this.withholdingTable[421900] = (num) => { return 111818.50 + ((num - 421900) * .35); };
                        this.withholdingTable[475500] = (num) => { return 130578.50 + ((num - 475500) * .396); };
                        break;
                }
                break;
        }

        return this.withholdingTable;
    }
}