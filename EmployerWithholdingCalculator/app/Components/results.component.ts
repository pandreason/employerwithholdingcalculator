﻿import { Component, Input, } from "@angular/core";
import { WithholdingCalculatorData } from "../SupportingClasses/WithholdingCalculatorData";

@Component({
    selector: 'results',
    templateUrl: 'ComponentTemplates/Results.Template.html'
})
export class ResultsComponent {
    @Input() withholdingCalculatorData: WithholdingCalculatorData;
}