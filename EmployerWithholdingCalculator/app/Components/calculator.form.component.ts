﻿import { Component, OnInit, Output, Input, EventEmitter } from "@angular/core";
import { FilingStatusesService } from "../Services/Filing.Statuses.Service";
import { TaxWitholdingCalculatorService } from "../Services/Tax.Withholding.Calculator.Service";
import { WithholdingAllowancesMap } from "../SupportingClasses/WithholdingAllowancesMap";
import { WithholdingCalculatorData } from "../SupportingClasses/WithholdingCalculatorData";
import { WithholdingAllowanceService } from "../Services/Withholding.Allowance.Service";
import { WithholdingTableService } from "../Services/Withholding.Table.Service";

@Component({
    selector:'calculator',
    templateUrl: '../ComponentTemplates/Calculator.Form.Template.html',
    providers: [FilingStatusesService, TaxWitholdingCalculatorService, WithholdingAllowanceService, WithholdingTableService]
})

export class CalculatorComponent implements OnInit {
    private filingStatuses: string[];
    private payFrequencies: string[];

    @Input() public withholdingCalculatorData: WithholdingCalculatorData;
    @Output() witholdingDataChange = new EventEmitter();

    ngOnInit(): void {
        this.getStatuses();
        this.calculate();
        this.getPayFrequencies();
    }

    constructor(private statusesService: FilingStatusesService,
                private calculatorService: TaxWitholdingCalculatorService,
                private withholdingService: WithholdingAllowanceService) { }    

    getStatuses(): void {
        this.filingStatuses = this.statusesService.getStatuses();
    }

    getPayFrequencies(): void {
        this.payFrequencies = this.withholdingService.getPayFrequencies();
    }

    changeFilingStatus(newvalue: string): void  {
        this.withholdingCalculatorData.taxFilingStatus = newvalue;
    }

    changePayFrequency(newvalue: string): void  {
        this.withholdingCalculatorData.payFrequency = newvalue;
    }

    calculate(): void {
        this.calculatorService.calculateWithholding(this.withholdingCalculatorData);
    }
}