import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CalculatorComponent } from './Components/calculator.form.component';
import { ResultsComponent } from './Components/results.component';

@NgModule({
    imports: [BrowserModule, FormsModule],
    declarations: [AppComponent, CalculatorComponent, ResultsComponent],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
