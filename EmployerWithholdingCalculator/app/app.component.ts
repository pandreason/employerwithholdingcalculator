import { Component } from '@angular/core';
import { CalculatorComponent } from './Components/calculator.form.component';
import { ResultsComponent } from './Components/results.component';
import { WithholdingCalculatorData } from "./SupportingClasses/WithholdingCalculatorData";

@Component({
    selector: 'employer-witholding-calculator',
    templateUrl: 'ComponentTemplates/App.Template.html',
    providers: [WithholdingCalculatorData]
})
export class AppComponent {
    constructor(public myWithholdingCalculatorData: WithholdingCalculatorData) { }
}
